# Thought Logger

An app to help you explore your thoughts and emotions.

## Instructions

1. Clone this repo
2. Run `npm install`
3. Run `npm start`, **localhost:8081** will open up in your default browser

## Tools used

For this project I used Webpack as my build tool, React for the front end, Sass for styling, and Eslint (extended with the Airbnb config) as my linter.
Lodash was used for a utility function and UUID to generate unique keys for mapping arrays.