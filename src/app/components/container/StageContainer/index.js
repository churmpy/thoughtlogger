import React, { Component } from 'react';
import PropTypes from 'prop-types';
import QuestionContainer from '../../presentation/QuestionContainer';
import TextInput from '../../presentation/TextInput';
import ButtonContainer from '../../presentation/ButtonContainer';

class StageContainer extends Component {
  constructor(props) {
    super(props);

    const {
      addInput,
      onContinue,
      onSearch,
      onComplete,
    } = this.props;

    this.state = {
      stageOne: {
        question: "What's on your mind?",
        questionInfo: "E.g. 'I made a fool of myself at that party. What are they going to think of me?'",
        buttonProps: [
          {
            type: 'button',
            text: 'Add',
            color: '#fff',
            backgroundColor: '#337ab7',
            borderColor: '#2e6da4',
            onClick: addInput,
          },
          {
            type: 'button',
            text: 'Continue',
            color: '#fff',
            backgroundColor: '#5cb85c',
            borderColor: '#4cae4c',
            onClick: onContinue,
          },
        ],
      },
      stageTwo: {
        question: 'How are you feeling?',
        questionInfo: 'E.g. embarrassed, ashamed, paranoid, afraid',
        buttonProps: [
          {
            type: 'button',
            text: 'Add',
            color: '#fff',
            backgroundColor: '#337ab7',
            borderColor: '#2e6da4',
            onClick: addInput,
          },
          {
            type: 'button',
            text: 'Find similar words',
            color: '#fff',
            backgroundColor: '#337ab7',
            borderColor: '#2e6da4',
            onClick: onSearch,
          },
          {
            type: 'button',
            text: 'Complete',
            color: '#fff',
            backgroundColor: '#5cb85c',
            borderColor: '#4cae4c',
            onClick: onComplete,
          },
        ],
      },
    };
  }

  render() {
    const {
      stage,
      value,
      onTextInputChange,
    } = this.props;
    const {
      [stage]: {
        question,
        questionInfo,
        buttonProps,
      },
    } = this.state;

    return (
      <div>
        <QuestionContainer
          question={question}
          questionInfo={questionInfo}
        />
        <TextInput
          value={value}
          onTextInputChange={onTextInputChange}
        />
        <ButtonContainer buttonProps={buttonProps} />
      </div>
    );
  }
}

StageContainer.propTypes = {
  stage: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onTextInputChange: PropTypes.func.isRequired,
  addInput: PropTypes.func.isRequired,
  onContinue: PropTypes.func.isRequired,
  onSearch: PropTypes.func.isRequired,
  onComplete: PropTypes.func.isRequired,
};

export default StageContainer;
