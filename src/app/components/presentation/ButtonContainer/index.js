import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import Button from '../Button';
import './button-container.scss';

class ButtonContainer extends Component {
  render() {
    const { buttonProps } = this.props;

    return (
      <div className="button-container">
        {
          buttonProps && buttonProps.map(button => (
            <Button
              key={uuid()}
              type={button.type}
              text={button.text}
              color={button.color}
              backgroundColor={button.backgroundColor}
              borderColor={button.borderColor}
              onClick={button.onClick}
            />
          ))
        }
      </div>
    );
  }
}

ButtonContainer.propTypes = {
  buttonProps: PropTypes.instanceOf(Object).isRequired,
};

export default ButtonContainer;
