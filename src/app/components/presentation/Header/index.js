import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './header.scss';

class Header extends Component {
  render() {
    const { stage } = this.props;

    let step = null;
    if (stage === 'stageOne') {
      step = <div>Step 1 of 2</div>;
    }
    if (stage === 'stageTwo') {
      step = <div>Step 2 of 2</div>;
    }

    return (
      <header className="header">
        <div className="header__company-name">Thought Logger</div>
        <div className="header__journey-progress">{step}</div>
      </header>
    );
  }
}

Header.propTypes = {
  stage: PropTypes.string.isRequired,
};

export default Header;
