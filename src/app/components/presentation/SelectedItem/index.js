import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './selected-item.scss';

class SelectedItem extends Component {
  render() {
    const {
      item,
      removeItem,
    } = this.props;

    return (
      <div className="selected-item">
        <div className="selected-item__text">{item}</div>
        <div
          className="selected-item__remove"
          role="button"
          onClick={() => removeItem(item)}
          tabIndex={0}
        >
          <svg width="24" height="24" viewBox="0 0 24 24">
            <path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z" />
            <path d="M0 0h24v24H0z" fill="none" />
          </svg>
        </div>
      </div>
    );
  }
}

SelectedItem.propTypes = {
  item: PropTypes.string.isRequired,
  removeItem: PropTypes.func.isRequired,
};

export default SelectedItem;
