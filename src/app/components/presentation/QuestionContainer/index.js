import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './question-container.scss';

class QuestionContainer extends Component {
  render() {
    const {
      question,
      questionInfo,
    } = this.props;

    return (
      <div className="question">
        <p className="question__title">{question}</p>
        <p className="question__info">{questionInfo}</p>
      </div>
    );
  }
}

QuestionContainer.propTypes = {
  question: PropTypes.string.isRequired,
  questionInfo: PropTypes.string.isRequired,
};

export default QuestionContainer;
