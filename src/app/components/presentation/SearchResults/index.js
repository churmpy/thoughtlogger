import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import Button from '../Button';
import './search-results.scss';

class SearchResults extends Component {
  render() {
    const {
      searchResults,
      selectItem,
    } = this.props;

    return (
      <div className="search-results">
        <div className="search-results__title">
          <p>Select the emotions that best fit</p>
        </div>
        <div className="search-results__items">
          <ul>
            {
              searchResults.map(item => (
                <li key={uuid()}>
                  <Button
                    type="button"
                    text={item.word}
                    color="#000"
                    backgroundColor="antiquewhite"
                    borderColor="#000"
                    onClick={() => selectItem(item.word)}
                  />
                </li>
              ))
            }
          </ul>
        </div>
      </div>
    );
  }
}

SearchResults.propTypes = {
  searchResults: PropTypes.instanceOf(Array).isRequired,
  selectItem: PropTypes.func.isRequired,
};

export default SearchResults;
