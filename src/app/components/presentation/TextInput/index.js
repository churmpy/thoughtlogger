import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './text-input.scss';

class TextInput extends Component {
  render() {
    const {
      value,
      onTextInputChange,
    } = this.props;

    return (
      <input
        type="text"
        className="input__text"
        value={value}
        onChange={onTextInputChange}
      />
    );
  }
}

TextInput.propTypes = {
  value: PropTypes.string.isRequired,
  onTextInputChange: PropTypes.func.isRequired,
};

export default TextInput;
