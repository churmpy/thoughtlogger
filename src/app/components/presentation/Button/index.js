import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './button.scss';

class Button extends Component {
  render() {
    const {
      type,
      text,
      color,
      backgroundColor,
      borderColor,
      onClick,
      disabled,
    } = this.props;

    const buttonStyle = {
      color,
      backgroundColor,
      borderColor,
    };

    return (
      <button
        type={type}
        className="button"
        onClick={onClick}
        style={buttonStyle}
        disabled={disabled}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  type: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  borderColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

Button.defaultProps = {
  disabled: false,
};

export default Button;
