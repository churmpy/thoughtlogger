import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import './summary.scss';

class Summary extends Component {
  render() {
    const {
      thoughts,
      emotions,
    } = this.props;

    return (
      <div className="summary">
        <p className="summary__title">Your summary</p>
        <div className="summary__results__container">
          <div className="summary__results">
            <p className="summary__results__title">You had these thoughts</p>
            <div className="summary__results__list">
              <ul>
                {
                  thoughts.map(thought => <li key={uuid()}>{thought}</li>)
                }
              </ul>
            </div>
          </div>
          <div className="summary__results">
            <p className="summary__results__title">While you felt this way</p>
            <div className="summary__results__list">
              <ul>
                {
                  emotions.map(emotion => <li key={uuid()}>{emotion}</li>)
                }
              </ul>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Summary.propTypes = {
  thoughts: PropTypes.instanceOf(Array).isRequired,
  emotions: PropTypes.instanceOf(Array).isRequired,
};

export default Summary;
