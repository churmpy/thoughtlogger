import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import SelectedItem from '../SelectedItem';
import './selected-item-container.scss';

class SelectedItemContainer extends Component {
  render() {
    const {
      selectedItem,
      removeItem,
    } = this.props;

    return (
      <div className="selected-item-container">
        <ul>
          {
            selectedItem && selectedItem.map(item => (
              <li key={uuid()}>
                <SelectedItem
                  item={item}
                  removeItem={removeItem}
                />
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}

SelectedItemContainer.propTypes = {
  selectedItem: PropTypes.instanceOf(Array).isRequired,
  removeItem: PropTypes.func.isRequired,
};

export default SelectedItemContainer;
