import React, { Component } from 'react';
import _ from 'lodash';
import StageContainer from './components/container/StageContainer';
import SelectedItemContainer from './components/presentation/SelectedItemContainer';
import SearchResults from './components/presentation/SearchResults';
import Summary from './components/presentation/Summary';
import Header from './components/presentation/Header';

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      stage: 'stageOne',
      input: '',
      searchResults: [],
      thoughts: [],
      emotions: [],
    };

    this.onTextInputChange = this.onTextInputChange.bind(this);
    this.onContinue = this.onContinue.bind(this);
    this.onSearch = this.onSearch.bind(this);
    this.onComplete = this.onComplete.bind(this);
    this.addInput = this.addInput.bind(this);
    this.removeItem = this.removeItem.bind(this);
    this.selectItem = this.selectItem.bind(this);
  }

  onTextInputChange(event) {
    const { value } = event.target;

    this.setState({ input: value });
  }

  onContinue() {
    const { thoughts } = this.state;

    if (!thoughts.length) { return; }

    this.setState({
      stage: 'stageTwo',
      input: '',
    });
  }

  onSearch() {
    const { input } = this.state;
    const baseURL = 'https://api.datamuse.com/words?rel_syn=';
    const url = baseURL + input;
    const that = this;

    if (!input.length) { return; }

    fetch(url, {
      method: 'GET',
    })
      .then(response => response.json())
      .then((searchResults) => {
        const searchedTerm = {
          word: input,
        };

        that.setState({
          input: '',
          searchResults: [searchedTerm, ...searchResults],
        });
      })
      .catch(error => console.log('error', error));
  }

  onComplete() {
    const {
      thoughts,
      emotions,
    } = this.state;

    if (!thoughts.length || !emotions.length) { return; }

    this.setState({
      stage: 'complete',
    });
  }

  addInput() {
    const {
      input,
      stage,
      thoughts,
      emotions,
    } = this.state;

    if (!input.length) { return; }
    if (stage === 'stageOne') {
      this.setState({
        input: '',
        thoughts: [...thoughts, input],
      });
    }
    if (stage === 'stageTwo') {
      this.setState({
        input: '',
        emotions: [...emotions, input],
      });
    }
  }

  removeItem(item) {
    const {
      thoughts,
      emotions,
      stage,
    } = this.state;

    if (stage === 'stageOne') {
      const updatedThoughts = _.pull(thoughts, item);

      this.setState({
        thoughts: updatedThoughts,
      });
    }
    if (stage === 'stageTwo') {
      const updatedEmotions = _.pull(emotions, item);

      this.setState({
        emotions: updatedEmotions,
      });
    }
  }

  selectItem(word) {
    const { emotions } = this.state;

    this.setState({
      emotions: [...emotions, word],
    });
  }

  render() {
    const {
      stage,
      input,
      thoughts,
      searchResults,
      emotions,
    } = this.state;
    let currentSelectedItem;

    if (stage === 'stageOne') {
      currentSelectedItem = thoughts;
    }
    if (stage === 'stageTwo') {
      currentSelectedItem = emotions;
    }
    if (stage === 'complete') {
      return (
        <div>
          <Header stage={stage} />
          <Summary
            thoughts={thoughts}
            emotions={emotions}
          />
        </div>
      );
    }

    return (
      <div>
        <Header stage={stage} />
        <StageContainer
          stage={stage}
          value={input}
          onTextInputChange={this.onTextInputChange}
          addInput={this.addInput}
          onContinue={this.onContinue}
          onComplete={this.onComplete}
          onSearch={this.onSearch}
        />
        {
          searchResults.length && stage === 'stageTwo'
            ? (
              <SearchResults
                searchResults={searchResults}
                input={input}
                selectItem={this.selectItem}
              />
            )
            : null
        }
        <SelectedItemContainer
          selectedItem={currentSelectedItem}
          removeItem={this.removeItem}
        />
      </div>
    );
  }
}

export default App;
